package com.i4digital.prueba.negocio;

import java.util.List;

import com.i4digital.prueba.model.Employee;

public interface EmployeeService {
	List<Employee> findAll();
}
